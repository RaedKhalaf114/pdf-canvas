FROM node

WORKDIR /app/frontend
ENV PATH /app/frontend/node_modules/.bin:$PATH
COPY package.json /app/frontend
COPY package-lock.json /app/frontend

RUN npm install -g --unsafe-perm node-sass
RUN npm install react-scripts@3.4.1 -g --silent

COPY . /app/frontend

EXPOSE 3000
CMD ["npm", "start"]

