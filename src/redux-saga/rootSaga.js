import { all } from "redux-saga/effects";
import AuthSaga from "components/Auth/sagas";
import DashboardSaga from "components/Dashboard/sagas";
import SettingSaga from "components/Settings/sagas";

export function* rootSaga() {
  yield all([AuthSaga(), DashboardSaga(), SettingSaga()]);
}
