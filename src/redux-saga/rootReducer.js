import { combineReducers } from "redux";
import AuthReducer from "components/Auth/reducer";
import DashboardReducer from "components/Dashboard/reducer";
import SharedComponentReducer from "components/shared/components/reducer";
import SettingsReducer from "components/Settings/reducer";

export default combineReducers({
  auth: AuthReducer,
  dashboard: DashboardReducer,
  sharedComponents: SharedComponentReducer,
  settings: SettingsReducer,
});
