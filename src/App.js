import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import "App.scss";
import { PrivateRoute } from "components/shared/components/PrivateRoute/PrivateRoute";
import Auth from "components/Auth";
import Snackbar from "components/shared/components/Snackbar/Snackbar";
import Dashboard from "components/Dashboard";
import Settings from "components/Settings";
import Header from "components/shared/components/Header";

function App() {
  return (
    <>
      <Switch>
        <PrivateRoute
          path="/home"
          component={() => {
            return (
              <>
                <Header />
                <PrivateRoute
                  path="/home"
                  exact
                  component={() => <Redirect to="/home/dashboard" />}
                />
                <PrivateRoute path="/home/dashboard" component={Dashboard} />
                <PrivateRoute path="/home/settings" component={Settings} />
              </>
            );
          }}
        />
        <Route path="/auth" component={Auth} />
        <Route path="/*" render={() => <Redirect to="/home" />} />
      </Switch>
      <Snackbar />
    </>
  );
}

export default App;
