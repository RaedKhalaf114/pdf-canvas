import { all, call, takeLatest, put, select } from "redux-saga/effects";

import * as ActionTypes from "./actionTypes";
import { doDelete, doGet, doPost } from "../../http/methods";
import {
  addFileEndpoint,
  pointsEndpoint,
  getFilePointsEndpoint,
  getImageEndpoint,
  imageSearchEndpoint,
  deleteFileEndpoint,
} from "./endpoints";
import {
  searchImages,
  searchImagesSuccess,
  triggerLoading,
  updateElements,
} from "./actions";
import { toBase64File } from "./utils";
import { activeElementsSelector } from "./selectors";
import { createElement } from "./UI/Sketch/utils/utilityFunctions";
import { triggerSnackbar } from "../shared/components/actions";

function* doGetObject({ payload }) {
  const { id, onSuccess, onFail } = payload;

  try {
    const fileResponse = yield call(doGet, { url: getImageEndpoint(id) + "" });
    const pointsResponse = yield call(doGet, {
      url: getFilePointsEndpoint(id) + "",
    });

    const points = pointsResponse?.data;

    if (points && points.length > 0) {
      createElement();
      const elements = [];
      const getPointType = (point) =>
        point.x1 === point.x2 || point.y1 === point.y2 ? "line" : "rectangle";
      points.forEach((point, index) =>
        elements.push(
          createElement(
            index,
            point.x1,
            point.y1,
            point.x2,
            point.y2,
            getPointType(point)
          )
        )
      );
      yield put(updateElements(elements));
    } else {
      yield put(updateElements([]));
    }

    if (onSuccess) {
      onSuccess(fileResponse.data);
    }
  } catch (e) {
    if (onFail) {
      onFail();
    }
  }
}

function* doObjectSearch({ payload }) {
  const response = yield call(doGet, { url: imageSearchEndpoint(payload) });

  yield put(searchImagesSuccess(response?.data));
}

function* addFile({ payload }) {
  const { groupId, name, file, size, type, onSuccess, onError } = payload;

  let fileContent = yield toBase64File(file);

  const requestBody = {
    name,
    content: fileContent,
    type,
    size,
    groupId,
  };

  try {
    const response = yield call(doPost, {
      url: addFileEndpoint,
      payload: requestBody,
    });
    if (onSuccess) {
      onSuccess(response);
    }
  } catch (e) {
    if (onError) {
      onError(e);
    }
  }
}

function* savePoints({ payload }) {
  yield put(triggerLoading(true));

  const elements = yield select(activeElementsSelector);

  const reqBody = {
    Points: [],
  };

  elements.forEach((element) => {
    reqBody.Points.push({
      x1: element.x1,
      x2: element.x2,
      y1: element.y1,
      y2: element.y2,
      fileId: payload,
    });
  });

  try {
    const response = yield call(doPost, {
      url: pointsEndpoint,
      payload: reqBody,
    });
  } catch (e) {
    console.error(e);
  } finally {
    yield put(triggerLoading(false));
  }
}

function* getPoints() {}

function* deletePoints({ payload }) {
  try {
    yield put(triggerLoading(true));

    const response = yield call(doDelete, {
      url: pointsEndpoint,
      payload: { fileId: payload },
    });
    yield put(updateElements([]));
  } catch (e) {
    console.error(e);
  } finally {
    yield put(triggerLoading(false));
  }
}

function* createPointsRelations() {}

function* deletePointsRelations() {}

function* deleteFile({ payload }) {
  try {
    const url = deleteFileEndpoint;
    const reqBody = {
      fileId: payload,
    };

    yield triggerLoading(true);

    const response = yield call(doDelete, { url, payload: reqBody });
    yield put(
      triggerSnackbar(true, `File with id = ${payload} deleted successfully`)
    );
    yield put(searchImages(0));
  } catch (e) {
    console.error(e);
    yield put(
      triggerSnackbar(true, `Couldn't remove the file, please try again`)
    );
  } finally {
    yield triggerLoading(false);
  }
}

export default function* () {
  yield all([
    takeLatest(ActionTypes.SEARCH_IMAGES, doObjectSearch),
    takeLatest(ActionTypes.GET_IMAGE, doGetObject),
    takeLatest(ActionTypes.ADD_FILE, addFile),
    takeLatest(ActionTypes.SAVE_POINTS, savePoints),
    takeLatest(ActionTypes.GET_POINTS, getPoints),
    takeLatest(ActionTypes.DELETE_POINTS, deletePoints),
    takeLatest(ActionTypes.CREATE_POINTS_RELATIONS, createPointsRelations),
    takeLatest(ActionTypes.DELETE_POINTS_RELATIONS, deletePointsRelations),
    takeLatest(ActionTypes.DELETE_FILE, deleteFile),
  ]);
}
