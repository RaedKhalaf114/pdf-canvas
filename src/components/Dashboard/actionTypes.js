export const TRIGGER_LOADING = "triggerLoading";
export const UPDATE_SELECTED_ELEMENT = "Update selected Element";

export const SEARCH_IMAGES = "Search images";
export const SEARCH_IMAGE_SUCCESS = "Search Images Success";
export const SEARCH_IMAGE_FAIL = "Search Images Fail";

export const GET_IMAGE = "Get Image";
export const GET_IMAGE_SUCCESS = "Get Image Success";
export const GET_IMAGE_FAIL = "Get Image Fail";

export const UPDATE_ACTIVE_TOOL = "Update active tool";
export const UPDATE_ELEMENTS = "Update Elements";
export const TRIGGER_SELECTION = "Trigger Selection";

export const REDO = "redo";
export const UNDO = "undo";
export const PUSH_HISTORY = "push history";

export const ADD_FILE = "Add File";
export const DELETE_FILE = "Delete File";

export const SAVE_POINTS = "Save Points";
export const GET_POINTS = "Get Points";
export const DELETE_POINTS = "Delete Points";
export const CREATE_POINTS_RELATIONS = "Create Points Relations";
export const DELETE_POINTS_RELATIONS = "Delete Points Relations";
