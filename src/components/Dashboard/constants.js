export const SketchTools = {
  LINE: "line",
  BOX: "rectangle",
  SELECTION: "selection",
  RESIZE: "Resize",
  MOVE: "Move",
  DELETE: "Delete",
  DRAWING: "Drawing",
};

export const SketchActions = {
  UNDO: "undo",
  REDO: "redo",
};
