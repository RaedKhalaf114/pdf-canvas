import * as ActionTypes from "./actionTypes";

export const triggerLoading = (payload) => ({
  type: ActionTypes.TRIGGER_LOADING,
  payload,
});

export const searchImages = (payload) => ({
  type: ActionTypes.SEARCH_IMAGES,
  payload,
});

export const searchImagesSuccess = (payload) => ({
  type: ActionTypes.SEARCH_IMAGE_SUCCESS,
  payload,
});

export const searchImagesFail = (payload) => ({
  type: ActionTypes.SEARCH_IMAGE_FAIL,
  payload,
});

export const getImage = (payload) => ({
  type: ActionTypes.GET_IMAGE,
  payload,
});

export const updateSketchTool = (payload) => ({
  type: ActionTypes.UPDATE_ACTIVE_TOOL,
  payload,
});

export const updateElements = (payload) => ({
  type: ActionTypes.UPDATE_ELEMENTS,
  payload,
});

export const triggerSelection = (payload) => ({
  type: ActionTypes.TRIGGER_SELECTION,
  payload,
});

export const redoAction = (payload) => ({
  type: ActionTypes.REDO,
  payload,
});

export const undoAction = (payload) => ({
  type: ActionTypes.UNDO,
  payload,
});

export const storeHistory = (payload) => ({
  type: ActionTypes.PUSH_HISTORY,
  payload,
});

export const addFile = (payload) => ({
  type: ActionTypes.ADD_FILE,
  payload,
});

export const savePoints = (payload) => ({
  type: ActionTypes.SAVE_POINTS,
  payload,
});

export const getPoints = (payload) => ({
  type: ActionTypes.GET_POINTS,
  payload,
});

export const deletePoints = (payload) => ({
  type: ActionTypes.DELETE_POINTS,
  payload,
});

export const createPointsRelations = (payload) => ({
  type: ActionTypes.CREATE_POINTS_RELATIONS,
  payload,
});

export const deletePointsRelations = (payload) => ({
  type: ActionTypes.DELETE_POINTS_RELATIONS,
  payload,
});

export const updateSelectedElement = (payload) => ({
  type: ActionTypes.UPDATE_SELECTED_ELEMENT,
  payload,
});

export const deleteFile = (payload) => ({
  type: ActionTypes.DELETE_FILE,
  payload,
});
