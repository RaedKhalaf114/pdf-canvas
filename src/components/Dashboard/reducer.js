import * as ActionTypes from "./actionTypes";
import { SketchTools } from "./constants";

const initialState = {
  isLoading: true,
  objects: [],
  pageCount: 0,
  activeObjectElements: [],
  activeSketchTool: SketchTools.LINE,
  isSelectionActive: false,
  elementsHistory: [],
  toBeRedo: [],
  selectedElement: undefined,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case ActionTypes.TRIGGER_LOADING:
      return { ...state, isLoading: payload };
    case ActionTypes.GET_IMAGE:
      return { ...state };
    case ActionTypes.SEARCH_IMAGES:
      return { ...state, isLoading: true };
    case ActionTypes.SEARCH_IMAGE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        objects: payload.files,
        pageCount: payload.countOfPages,
      };
    case ActionTypes.UPDATE_ACTIVE_TOOL:
      return { ...state, activeSketchTool: payload };
    case ActionTypes.UPDATE_ELEMENTS:
      return { ...state, activeObjectElements: payload, toBeRedo: [] };
    case ActionTypes.TRIGGER_SELECTION:
      return { ...state, isSelectionActive: payload };
    case ActionTypes.ADD_FILE:
      return { ...state, isLoading: true };
    case ActionTypes.PUSH_HISTORY:
      return { ...state, elementsHistory: [payload, ...state.elementsHistory] };
    case ActionTypes.REDO:
      if (state.toBeRedo.length > 0) {
        const newToBeRedo = state.toBeRedo.slice(1);
        const toBeRedoElements = state.toBeRedo[0];
        return {
          ...state,
          toBeRedo: newToBeRedo,
          activeObjectElements: toBeRedoElements,
          elementsHistory: [toBeRedoElements, ...state.elementsHistory],
        };
      }
      return { ...state };
    case ActionTypes.UNDO:
      if (state.elementsHistory.length > 0) {
        const newHistory = state.elementsHistory.slice(1);
        let previousHistory = [];
        if (newHistory.length > 0) {
          previousHistory = newHistory[0];
        }
        return {
          ...state,
          elementsHistory: newHistory,
          activeObjectElements: previousHistory,
          toBeRedo: [state.elementsHistory[0], ...state.toBeRedo],
        };
      }
      return { ...state };
    case ActionTypes.UPDATE_SELECTED_ELEMENT:
      return { ...state, selectedElement: payload };
    default:
      return state;
  }
};
