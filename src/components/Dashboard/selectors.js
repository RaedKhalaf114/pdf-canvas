import { createSelector } from "reselect";

export const dashboardStateSelector = (state) => state.dashboard;

export const isLoadingSelector = createSelector(
  dashboardStateSelector,
  (state) => state.isLoading
);

export const objectsSelector = createSelector(
  dashboardStateSelector,
  (state) => state.objects
);

export const pageCountSelector = createSelector(
  dashboardStateSelector,
  (state) => state.pageCount
);

export const activeToolSelector = createSelector(
  dashboardStateSelector,
  (state) => state.activeSketchTool
);

export const activeElementsSelector = createSelector(
  dashboardStateSelector,
  (state) => state.activeObjectElements
);

export const isSelectionActiveSelector = createSelector(
  dashboardStateSelector,
  (state) => state.isSelectionActive
);

export const selectedElementSelector = createSelector(
  dashboardStateSelector,
  (state) => state.selectedElement
);
