export const imageSearchEndpoint = (pageNumber) => `/files/?page=${pageNumber}`;
export const getImageEndpoint = (id) => `/file/${id}`;
export const getFilePointsEndpoint = (id) => `/point/${id}`;
export const pointsEndpoint = "/point/";

export const addFileEndpoint = "/file/";
export const deleteFileEndpoint = "/file/";
