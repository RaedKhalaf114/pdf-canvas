import React, { useState } from "react";
import { connect } from "react-redux";
import { Row, Col } from "antd";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import Typography from "@material-ui/core/Typography";

import "./ObjectsList.scss";
import AddObject from "./AddObject/AddObject";
import { searchImages } from "../../actions";
import { triggerSnackbar } from "../../../shared/components/actions";

const ObjectListHeader = ({ searchImages, triggerSnackbar }) => {
  const [isAddObjectActive, setAddObject] = useState(false);

  const handleAddObject = () => {
    setAddObject(true);
  };

  const handleAddObjectClose = (status) => {
    setAddObject(false);

    if (status === undefined) {
      return;
    }

    triggerSnackbar(
      true,
      Boolean(status)
        ? "File is added successfully"
        : "Something went wrong, please try again"
    );

    if (Boolean(status)) {
      searchImages(1);
    }
  };

  return (
    <Row className="body-header">
      <Col>
        <Typography variant="h5" gutterBottom>
          Your Objects
        </Typography>
      </Col>
      <Col>
        <Button
          variant="contained"
          onClick={handleAddObject}
          startIcon={<AddIcon />}
          color="primary"
        >
          Object
        </Button>
        <AddObject open={isAddObjectActive} onClose={handleAddObjectClose} />
      </Col>
    </Row>
  );
};

export default connect(undefined, { searchImages, triggerSnackbar })(
  ObjectListHeader
);
