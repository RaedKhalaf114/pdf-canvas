import React from "react";
import ObjectListHeader from "./ObjectListHeader";
import Container from "@material-ui/core/Container";
import ObjectsTable from "./ObjectsTable/ObjectsTable";

const Objects = ({ onSelectObj }) => (
  <Container className="objects-container" maxWidth="lg">
    <ObjectListHeader />
    <ObjectsTable onSelectObj={onSelectObj} />
  </Container>
);

export default Objects;
