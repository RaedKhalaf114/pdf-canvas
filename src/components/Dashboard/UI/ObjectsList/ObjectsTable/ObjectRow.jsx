import React, { useCallback, useState } from "react";
import { connect } from "react-redux";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

import "./ObjectsTable.scss";
import ConfirmationDialog from "../../../../shared/components/ConfirmationDialog";
import { deleteFile } from "../../../actions";
import RowAction from "../../../../shared/components/RowActions";

const ObjectRow = ({ row, onSelectObj, deleteFile }) => {
  const [isHover, setHover] = useState(false);
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);

  const handleRowEdit = (e) => {
    e.stopPropagation();
  };

  const handleRowRemove = (e) => {
    e.stopPropagation();
    setShowConfirmDialog(true);
  };

  const onConfirmDelete = () => {
    setShowConfirmDialog(false);
    deleteFile(row.id);
  };

  return (
    <>
      <ConfirmationDialog
        isOpen={showConfirmDialog}
        onClose={() => setShowConfirmDialog(false)}
        onConfirm={onConfirmDelete}
        title={`Delete ${row?.name} file`}
        content={`Are you sure you want to delete this file ? this operation is not reversible.`}
      />
      <TableRow
        key={row.name}
        className="table-row"
        onClick={() => onSelectObj(row)}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <TableCell>
          <img
            alt="icon"
            src={
              process.env.PUBLIC_URL + row?.type?.toLowerCase() ===
              "application/pdf"
                ? "/images/pdf.png"
                : "/images/file.png"
            }
            width={30}
          />
        </TableCell>
        <TableCell>{row.name}</TableCell>
        <TableCell>{row.type}</TableCell>
        <TableCell>{row.size}</TableCell>
        <TableCell width={100}>
          {isHover && (
            <RowAction
              handleRowEdit={handleRowEdit}
              handleRowRemove={handleRowRemove}
            />
          )}
        </TableCell>
      </TableRow>
    </>
  );
};

export default connect(undefined, { deleteFile })(ObjectRow);
