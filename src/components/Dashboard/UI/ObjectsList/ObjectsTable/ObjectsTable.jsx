import React from "react";
import { connect } from "react-redux";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TablePaginationActions from "components/shared/components/TablePaginationActions";

import "./ObjectsTable.scss";
import { objectsSelector, pageCountSelector } from "../../../selectors";
import { searchImages } from "../../../actions";
import ObjectRow from "./ObjectRow";

const ObjectsTable = ({
  rows = [],
  pages,
  searchImages,
  onSelectObj = () => {},
}) => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const emptyRows = rowsPerPage - rows.length;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    searchImages(newPage + 1);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <TableContainer>
      <Table aria-label="custom pagination table">
        <TableHead className="table-header">
          <TableRow>
            <TableCell />
            <TableCell>Name</TableCell>
            <TableCell>Type</TableCell>
            <TableCell>Size</TableCell>
            <TableCell width={100} />
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <ObjectRow row={row} onSelectObj={onSelectObj} />
          ))}

          {rows?.length === 0 && (
            <div className="empty-table-label">No Files...</div>
          )}

          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[10]}
              colSpan={3}
              count={pages * 10}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { "aria-label": "rows per page" },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
};

const mapStateToProps = (state) => ({
  rows: objectsSelector(state),
  pages: pageCountSelector(state),
});

export default connect(mapStateToProps, { searchImages })(ObjectsTable);
