import React from "react";

import "./ObjectsList.scss";

const ObjectItem = ({ object, onSelect }) => {
  return (
    <div className="object-container" onClick={() => onSelect()}>
      <div className="image-container">
        <img src={object.content} />
      </div>
      <div className="description-container">
        <div className="image-description">{object.name}</div>
      </div>
    </div>
  );
};

export default ObjectItem;
