import React, { useMemo, useState } from "react";
import { connect } from "react-redux";

import "./AddObject.scss";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import { DropzoneArea } from "material-ui-dropzone";
import TextField from "@material-ui/core/TextField";
import { addFile } from "../../../actions";
import LoadingOverlay from "../../../../shared/components/LoadingOverlay/LoadingOverlay";
import { groupsSelector } from "../../../../Settings/selectors";
import Select from "react-select";

const AddObject = ({ open, onClose, addFile, isGroupsLoading, groups }) => {

  const [name, setName] = useState("");
  const [group, setGroup] = useState(undefined);
  const [files, setFiles] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const handleAddFileSuccess = () => {
    setName("");
    setLoading(false);
    onClose(true);
  };

  const handleAddFileError = (error) => {
    setLoading(false);
    onClose(false);
  };

  const handleAdd = () => {
    setLoading(true);
    addFile({
      name,
      file: files[0],
      size: files[0].size,
      type: files[0].type,
      groupId: group?.value,
      onSuccess: handleAddFileSuccess,
      onError: handleAddFileError,
    });
  };

  const handleChange = (files) => {
    setFiles(files);
  };

  const groupsOptions = useMemo(() => groups?.map((group) => ({ value: group.id, label: group.name }))
      , [groups]);

  return (
    <Dialog open={open} className="dialog-container">
      <LoadingOverlay
        isLoading={isLoading || isGroupsLoading}
        isFullScreen={false}
      />

      <DialogTitle>Add Object</DialogTitle>

      <DialogContent>
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          label="File name"
          type="text"
          autoFocus
          value={name}
          onChange={(e) => setName(e.target?.value)}
        />

        <Select options={groupsOptions} value={group} onChange={(newValue) => setGroup(newValue)} />

        <div className="dropzone-area">
          <DropzoneArea
            filesLimit={1}
            showPreviews={true}
            showPreviewsInDropzone={false}
            showFileNames={true}
            useChipsForPreview={true}
            onChange={handleChange}
            acceptedFiles={["image/jpeg", "application/pdf", "image/png"]}
          />
        </div>
      </DialogContent>

      <DialogActions>
        <Button onClick={() => onClose()} color="secondary">
          Cancel
        </Button>
        <Button
          onClick={handleAdd}
          color="primary"
          disabled={!(Boolean(name) && files?.length > 0)}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const mapStateToProps = (state) => ({
  groups: groupsSelector(state),
});

export default connect(mapStateToProps, { addFile })(AddObject);
