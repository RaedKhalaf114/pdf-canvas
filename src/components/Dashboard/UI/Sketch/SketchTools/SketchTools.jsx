import React from "react";
import { connect } from "react-redux";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import Crop32Icon from "@material-ui/icons/Crop32";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import PanToolIcon from "@material-ui/icons/PanTool";
import ZoomInIcon from "@material-ui/icons/ZoomIn";
import ZoomOutIcon from "@material-ui/icons/ZoomOut";
import CloseIcon from "@material-ui/icons/Close";
import UndoIcon from "@material-ui/icons/Undo";
import RedoIcon from "@material-ui/icons/Redo";
import Classnames from "classnames";

import "./SketchTools.scss";
import { SketchTools } from "../../../constants";
import { activeToolSelector } from "../../../selectors";
import { redoAction, undoAction, updateSketchTool } from "../../../actions";

const tools = [
  { icon: ArrowRightAltIcon, tooltip: "Draw Line", type: SketchTools.LINE },
  { icon: Crop32Icon, tooltip: "Draw Box", type: SketchTools.BOX },
  { icon: PanToolIcon, tooltip: "Move", type: SketchTools.SELECTION },
  { icon: ZoomOutIcon, tooltip: "Zoom In", type: "" },
  { icon: ZoomInIcon, tooltip: "Zoom Out", type: "" },
  // {icon: SelectAllIcon, tooltip: 'Select', type: ''},
  { icon: DeleteForeverIcon, tooltip: "Delete", type: SketchTools.DELETE },
];

const SketchTool = ({
  onClose,
  activeSketchTool,
  updateSketchTool,
  redoAction,
  undoAction,
  canRedo,
  canUndo,
}) => {
  return (
    <div className="sketch-tools">
      <div />

      <div className="actions">
        {tools.map(({ icon: Icon, tooltip, type }) => (
          <div
            className={Classnames("action", {
              selected: type === activeSketchTool,
            })}
            onClick={() => updateSketchTool(type)}
          >
            <Icon key={tooltip} aria-label={tooltip} titleAccess={tooltip} />
          </div>
        ))}
        <div
          className={Classnames("action", { "disabled-action": !canUndo })}
          onClick={() => undoAction()}
        >
          <UndoIcon aria-label="Undo" titleAccess={"undo"} />
        </div>
        <div
          className={Classnames("action", { "disabled-action": !canRedo })}
          onClick={() => redoAction()}
        >
          <RedoIcon aria-label="Redo" titleAccess={"Redo"} />
        </div>
      </div>

      <div className="close-icon">
        <CloseIcon onClick={onClose} />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  activeSketchTool: activeToolSelector(state),
  canRedo: state.dashboard.toBeRedo.length > 0,
  canUndo: state.dashboard.elementsHistory.length > 0,
});

export default connect(mapStateToProps, {
  updateSketchTool,
  undoAction,
  redoAction,
})(SketchTool);
