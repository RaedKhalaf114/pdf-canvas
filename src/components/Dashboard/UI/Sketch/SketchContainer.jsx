import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import Sketch from "./Sketch";

import "./Sketch.scss";
import SketchTool from "./SketchTools";
import SketchObjects from "./SketchObjects";
import { getImage } from "../../actions";
import LoadingOverlay from "../../../shared/components/LoadingOverlay/LoadingOverlay";
import { isLoadingSelector } from "../../selectors";
import { triggerSnackbar } from "../../../shared/components/actions";

const SketchContainer = ({
  getImage,
  onClose,
  selectedObject: activeObj,
  open,
  isLoadingFromSaga,
  triggerSnackbar,
}) => {
  const [isLoading, setLoading] = useState(false);
  const [selectedObject, setSelectedObject] = useState(activeObj);

  const handleClose = () => {
    onClose();
  };

  const onSuccess = (response) => {
    setSelectedObject(response);
    setLoading(false);
  };

  const onFail = () => {
    setLoading(false);
  };

  useEffect(() => {
    if (activeObj) {
      setLoading(true);
      getImage({ id: activeObj?.id, onSuccess, onFail });
    }
  }, [activeObj?.id]);

  if (isLoading) {
    return <LoadingOverlay isFullScreen={true} isLoading={true} />;
  }

  const enableSnackbar = (message) => {
    triggerSnackbar(true, message);
  };

  return (
    <>
      {isLoadingFromSaga && (
        <LoadingOverlay isFullScreen={true} isLoading={true} />
      )}
      <Dialog
        onClose={handleClose}
        className="sketch-dialog"
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <SketchTool onClose={onClose} />

        <div className="sketch-body">
          <div className="sketch">
            <Sketch img={selectedObject?.content} />
          </div>

          <SketchObjects
            onClose={onClose}
            fileId={selectedObject?.id}
            setMessage={enableSnackbar}
          />
        </div>
      </Dialog>
    </>
  );
};

const mapStateToProps = (state) => ({
  isLoadingFromSaga: isLoadingSelector(state),
});

export default connect(mapStateToProps, { getImage, triggerSnackbar })(
  SketchContainer
);
