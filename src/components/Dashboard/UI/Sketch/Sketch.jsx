import React, {
  useLayoutEffect,
  useState,
  useEffect,
  useMemo,
  useCallback,
} from "react";
import { connect } from "react-redux";
import { pdfjs } from "react-pdf";

import rough from "roughjs/bundled/rough.esm";
import {
  adjustElementCoordinates,
  createElement,
  createSelectedElement,
  getElementAtPosition,
  getPage,
  getXY,
  isSelectionOn as isSelectionOnUtil,
  resizedCoordinates,
} from "./utils/utilityFunctions";
import { SketchTools } from "../../constants";
import {
  activeElementsSelector,
  activeToolSelector,
  isSelectionActiveSelector,
  selectedElementSelector,
} from "../../selectors";
import {
  storeHistory,
  triggerSelection,
  updateElements,
  updateSelectedElement,
} from "../../actions";

const Sketch = ({
  img = "",
  activeSketchTool = SketchTools.LINE,
  elements = [],
  updateElements,
  triggerSelection,
  isSelectionActive,
  storeHistory,
  updateSelectedElement: setSelectedElement,
  selectedElement,
}) => {
  const [action, setAction] = useState("none");
  const [activeKeydown, setActiveKeydown] = useState(null);
  const [isMouseDown, setMouseDown] = useState(false);

  useEffect(() => {
    const canvas = document.getElementById("canvas");
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    canvas.style.cursor = "crosshair";
    drawElements();

    if (img && img.indexOf("application/pdf") !== -1) {
      pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
      const loadingTask = pdfjs.getDocument(img);
      loadingTask.promise
        .then((pdf) => {
          getPage(1, pdf).then((result) => {
            canvas.style.backgroundImage = `url(${result})`;
          });
        })
        .catch((erro) => console.log(erro));
    }
  }, []);

  useLayoutEffect(() => {
    drawElements();
  }, [elements, selectedElement]);

  const drawElements = useCallback(() => {
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);

    const roughCanvas = rough.canvas(canvas);

    elements.forEach((element) => {
      if (element) {
        if (selectedElement !== null && element?.id === selectedElement?.id) {
          element = createSelectedElement(element);
        } else {
          element.roughElement.options.stroke = "black";
        }
        roughCanvas.draw(element.roughElement);
      }
    });
  }, [elements, selectedElement]);

  const isSelectionOn = useMemo(
    () =>
      isSelectionOnUtil(
        activeKeydown,
        isSelectionActive,
        activeSketchTool,
        action
      ),
    [activeKeydown, isSelectionActive, action, activeSketchTool]
  );

  const updateElement = (id, x1, y1, x2, y2, type) => {
    const updatedElement = createElement(id, x1, y1, x2, y2, type);

    const elementsCopy = [...elements];
    elementsCopy[id] = updatedElement;
    updateElements(elementsCopy);
  };

  useEffect(() => {
    const onKeydown = (e) => {
      if (activeKeydown) {
        return;
      }

      switch (e.keyCode) {
        case 17:
          if (!isSelectionOn) {
            triggerSelection(true);
            setActiveKeydown(SketchTools.SELECTION);
          }
      }
    };

    const onKeyUp = (e) => {
      if (e.keyCode === 17) {
        triggerSelection(false);
        setActiveKeydown(undefined);
      }
    };

    window.addEventListener("keydown", onKeydown);
    window.addEventListener("keyup", onKeyUp);

    return () => {
      window.removeEventListener("keydown", onkeydown);
      window.removeEventListener("keyup", onkeyup);
    };
  }, []);

  const handleMouseDown = (event) => {
    if (event?.nativeEvent?.which !== 1) {
      return;
    }
    setMouseDown(true);

    const { clientX, clientY } = getXY(event);

    if (isSelectionOn) {
      const element = getElementAtPosition(clientX, clientY, elements);
      if (element) {
        const offsetX = clientX - element.x1;
        const offsetY = clientY - element.y1;
        setSelectedElement({ ...element, offsetX, offsetY });

        if (element.position === "inside") {
          setAction(SketchTools.MOVE);
        } else {
          setAction(SketchTools.RESIZE);
        }
      }
    } else if (activeSketchTool === SketchTools.DELETE) {
      const deleteElement = getElementAtPosition(clientX, clientY, elements);
      if (deleteElement) {
        updateElements(
          elements.filter((element) => deleteElement.id !== element.id)
        );
      }
    } else {
      const id = elements.length;
      const element = createElement(
        id,
        clientX,
        clientY,
        clientX,
        clientY,
        activeSketchTool
      );
      updateElements([...elements, element]);
      setSelectedElement(element);

      setAction(SketchTools.DRAWING);
    }
  };

  const handleMouseMove = (event) => {
    if (!isMouseDown) {
      return;
    }

    const { clientX, clientY } = getXY(event);

    if (action === SketchTools.DRAWING) {
      const index = elements.length - 1;
      const { x1, y1 } = elements[index];

      if (activeSketchTool === SketchTools.LINE) {
        const distanceToYAxis = Math.abs(clientY - y1);
        const distanceToXAxis = Math.abs(clientX - x1);

        if (distanceToYAxis > distanceToXAxis) {
          updateElement(index, x1, y1, x1, clientY, activeSketchTool);
        } else {
          updateElement(index, x1, y1, clientX, y1, activeSketchTool);
        }
      } else {
        updateElement(index, x1, y1, clientX, clientY, activeSketchTool);
      }
    } else if (action === SketchTools.MOVE) {
      const { id, x1, x2, y1, y2, type, offsetX, offsetY } = selectedElement;
      const width = x2 - x1;
      const height = y2 - y1;
      const newX1 = clientX - offsetX;
      const newY1 = clientY - offsetY;
      updateElement(id, newX1, newY1, newX1 + width, newY1 + height, type);
    } else if (action === SketchTools.RESIZE) {
      const { id, type, position, ...coordinates } = selectedElement;
      const { x1, y1, x2, y2 } = resizedCoordinates(
        clientX,
        clientY,
        position,
        coordinates
      );
      updateElement(id, x1, y1, x2, y2, type);
    }
  };

  const handleMouseUp = (event) => {
    if (event?.nativeEvent?.which !== 1) {
      return;
    }

    setMouseDown(false);

    storeHistory(elements);

    const index = selectedElement?.id;
    if (selectedElement && elements[index]) {
      const { id, type } = elements[index];
      if (action === SketchTools.DRAWING || action === SketchTools.RESIZE) {
        const { x1, y1, x2, y2 } = adjustElementCoordinates(elements[index]);
        updateElement(id, x1, y1, x2, y2, type, elements, updateElements);
      }

      setAction("none");
    }
  };

  return (
    <div className="sketch-container">
      <canvas
        style={{
          backgroundImage: `url(${img})`,
        }}
        id="canvas"
        onMouseDown={handleMouseDown}
        onMouseMove={handleMouseMove}
        onMouseUp={handleMouseUp}
      >
        Canvas
      </canvas>
    </div>
  );
};

const mapStateToProps = (state) => ({
  activeSketchTool: activeToolSelector(state),
  elements: activeElementsSelector(state),
  isSelectionActive: isSelectionActiveSelector(state),
  selectedElement: selectedElementSelector(state),
});

export default connect(mapStateToProps, {
  updateElements,
  triggerSelection,
  updateSelectedElement,
  storeHistory,
})(Sketch);
