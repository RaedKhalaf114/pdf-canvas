import rough from "roughjs/bundled/rough.esm";
import SketchObjects from "../SketchObjects";
import { SketchTools } from "../../../constants";
import { act } from "@testing-library/react";

export const generator = rough.generator();

export const createElement = (id, x1, y1, x2, y2, type) => {
  const roughElement =
    type === "line"
      ? generator.line(x1, y1, x2, y2, { roughness: 0 })
      : generator.rectangle(x1, y1, x2 - x1, y2 - y1, { roughness: 0 });
  return { id, x1, y1, x2, y2, type, roughElement };
};

export const createSelectedElement = (element) => {
  if (element.type === "line") {
    element.roughElement.options.stroke = "red";
  } else {
    const { id, x1, x2, y1, y2, type } = element;
    const rect = generator.rectangle(x1, y1, x2 - x1, y2 - y1, {
      roughness: 0,
      bowing: 6,
      stroke: "red",
      fill: "rgba(205,100,100,0.2)",
      strokeWidth: 3,
    });
    return { id, x1, y1, x2, y2, type, roughElement: rect };
  }

  return element;
};

export const nearPoint = (x, y, x1, y1, name) => {
  return Math.abs(x - x1) < 5 && Math.abs(y - y1) < 5 ? name : null;
};

export const positionWithinElement = (x, y, element) => {
  const { type, x1, x2, y1, y2 } = element;

  if (type === SketchTools.BOX) {
    const topLeft = nearPoint(x, y, x1, y1, "tl");
    const topRight = nearPoint(x, y, x2, y1, "tr");
    const bottomLeft = nearPoint(x, y, x1, y2, "bl");
    const bottomRight = nearPoint(x, y, x2, y2, "br");
    const inside = x >= x1 && x <= x2 && y >= y1 && y <= y2 ? "inside" : null;
    return topLeft || topRight || bottomLeft || bottomRight || inside;
  } else {
    const a = { x: x1, y: y1 };
    const b = { x: x2, y: y2 };
    const c = { x, y };
    const offset = distance(a, b) - (distance(a, c) + distance(b, c));
    const start = nearPoint(x, y, x1, y1, "start");
    const end = nearPoint(x, y, x2, y2, "end");
    const inside = Math.abs(offset) < 1 ? "inside" : null;
    return start || end || inside;
  }
};

export const distance = (a, b) =>
  Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));

export const getElementAtPosition = (x, y, elements) => {
  return elements
    .map((element) => ({
      ...element,
      position: positionWithinElement(x, y, element),
    }))
    .find((element) => element.position !== null);
};

export const adjustElementCoordinates = (element) => {
  const { type, x1, y1, x2, y2 } = element;
  if (type === "rectangle") {
    const minX = Math.min(x1, x2);
    const maxX = Math.max(x1, x2);
    const minY = Math.min(y1, y2);
    const maxY = Math.max(y1, y2);
    return { x1: minX, y1: minY, x2: maxX, y2: maxY };
  } else {
    if (x1 < x2 || (x1 === x2 && y1 < y2)) {
      return { x1, y1, x2, y2 };
    } else {
      return { x1: x2, y1: y2, x2: x1, y2: y1 };
    }
  }
};

export const cursorForPosition = (action, activeSketchTool, isSelectionOn) => {
  if (isSelectionOn) {
    return `all-scroll`;
  } else if (
    activeSketchTool === SketchTools.LINE ||
    activeSketchTool === SketchTools.BOX
  ) {
    return "crosshair";
  }

  return "default";
};

export const resizedCoordinates = (clientX, clientY, position, coordinates) => {
  const { x1, y1, x2, y2 } = coordinates;
  switch (position) {
    case "tl":
    case "start":
      return { x1: clientX, y1: clientY, x2, y2 };
    case "tr":
      return { x1, y1: clientY, x2: clientX, y2 };
    case "bl":
      return { x1: clientX, y1, x2, y2: clientY };
    case "br":
    case "end":
      return { x1, y1, x2: clientX, y2: clientY };
    default:
      return null;
  }
};

export const getXY = (event) => {
  let { clientX, clientY } = event;
  const canvas = document.getElementById("canvas");
  clientX = clientX - canvas.getBoundingClientRect().x;
  clientY = clientY - canvas.getBoundingClientRect().y;
  return { clientX, clientY };
};

export const isSelectionOn = (
  activeKeydown,
  isSelectionActive,
  activeSketchTool,
  activeAction
) => {
  const isSelectionOn =
    activeKeydown === SketchTools.SELECTION ||
    isSelectionActive ||
    activeSketchTool === SketchTools.SELECTION;

  const canvas = document.getElementById("canvas");
  if (canvas) {
    canvas.style.cursor = cursorForPosition(
      activeAction,
      activeSketchTool,
      isSelectionOn
    );
  }

  return isSelectionOn;
};

export const getPage = (num, pdf) => {
  return new Promise((resolve, reject) => {
    pdf.getPage(num).then((page) => {
      const scale = "1.5";
      const viewport = page.getViewport({
        scale: scale,
      });
      const canvas = document.createElement("canvas");
      const canvasContext = canvas.getContext("2d");
      canvas.height =
        viewport.height || viewport.viewBox[3]; /* viewport.height is NaN */
      canvas.width =
        viewport.width || viewport.viewBox[2]; /* viewport.width is also NaN */
      page
        .render({
          canvasContext,
          viewport,
        })
        .promise.then((res) => {
          resolve(canvas.toDataURL());
        });
    });
  });
};
