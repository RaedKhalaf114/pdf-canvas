import React from "react";
import { connect } from "react-redux";

import "./SketchObjects.scss";
import { activeElementsSelector } from "../../../selectors";
import Button from "@material-ui/core/Button";
import { deletePoints, savePoints } from "../../../actions";
import ObjectItem from "./ObjectItem";

const SketchObjects = ({
  elements,
  savePoints,
  deletePoints,
  fileId,
  setMessage,
}) => {
  const handleSavePoints = () => {
    savePoints(fileId);
    setMessage("Points saved successfully");
  };

  const handleDeletePoints = () => {
    deletePoints(fileId);
    setMessage("Points removed successfully");
  };

  return (
    <div className="sketch-objects">
      <div className="points-container">
        {elements?.map((element) => (
          <ObjectItem element={element} />
        ))}
        {elements?.length === 0 && (
          <div className="empty-list">No elements</div>
        )}
      </div>
      <div className="actions">
        <Button variant="contained" color="primary" onClick={handleSavePoints}>
          Save Points
        </Button>
        <Button
          variant="contained"
          color="secondary"
          onClick={handleDeletePoints}
        >
          Delete Points
        </Button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  elements: activeElementsSelector(state),
});

export default connect(mapStateToProps, { savePoints, deletePoints })(
  SketchObjects
);
