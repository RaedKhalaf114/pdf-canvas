import React from "react";

const LineIcon = () => (
  <div className="icon-container">
    <div className="line-icon"></div>
  </div>
);

export default LineIcon;
