import React from "react";

const RectIcon = () => (
  <div className="icon-container">
    <div className="rect-icon"></div>
  </div>
);

export default RectIcon;
