import React from "react";
import { connect } from "react-redux";
import Classnames from "classnames";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

import "../SketchObjects.scss";
import { SketchTools } from "../../../../constants";
import RectIcon from "./RectIcon";
import LineIcon from "./LineIcon";

import "./ObjectItem.scss";
import { updateSelectedElement } from "../../../../actions";
import { selectedElementSelector } from "../../../../selectors";

const ObjectItem = ({ element, updateSelectedElement, selectedElement }) => {
  return (
    <div
      className={Classnames("element-container", {
        "selected-element": selectedElement?.id === element.id,
      })}
      onClick={() => updateSelectedElement(element)}
    >
      <div className="element-icon">
        {element?.type === SketchTools.BOX ? <RectIcon /> : <LineIcon />}
      </div>

      <div className="element-details">
        <div className="element-type">
          Type - Value
        </div>
        <div className="element-coordinates">
          ({element?.x1}, {element?.y1}), ({element?.x2}, {element?.y2})
        </div>
      </div>

      <div className="element-actions">
        <DeleteForeverIcon />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  selectedElement: selectedElementSelector(state),
});

export default connect(mapStateToProps, { updateSelectedElement })(ObjectItem);
