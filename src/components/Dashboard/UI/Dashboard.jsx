import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import { searchImages } from "../actions";
import ObjectsList from "./ObjectsList";

import "./Dashboard.scss";
import { isLoadingSelector } from "../selectors";
import { isLoadingSelector as isGroupsLoadingSelector} from 'components/Settings/selectors';
import LoadingOverlay from "../../shared/components/LoadingOverlay/LoadingOverlay";
import SketchContainer from "./Sketch";
import {getGroups} from "../../Settings/actions";

const Dashboard = ({ searchImages, isLoading, getGroups }) => {
  const [selectedObject, setSelectedObject] = useState(undefined);

  useEffect(() => {
    searchImages(1);
    getGroups();
  }, []);

  return (
    <div className="dashboard-container">
      <ObjectsList onSelectObj={(obj) => setSelectedObject(obj)} />
      {isLoading && <LoadingOverlay isLoading={isLoading} />}
      <SketchContainer
        onClose={() => setSelectedObject(undefined)}
        open={Boolean(selectedObject)}
        selectedObject={selectedObject}
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  isLoading: isLoadingSelector(state) || isGroupsLoadingSelector(state),
});

export default connect(mapStateToProps, { searchImages, getGroups})(Dashboard);
