import React from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import { deepPurple } from "@material-ui/core/colors";
import PopupState, { bindTrigger, bindPopover } from "material-ui-popup-state";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Popover from "@material-ui/core/Popover";
import { useHistory } from "react-router-dom";

import "./Avatar.scss";
import { doLogout, logoutSuccess } from "../../../Auth/actions";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  purple: {
    color: theme.palette.getContrastText(deepPurple[500]),
    backgroundColor: deepPurple[500],
  },
}));

const UserAvatar = ({ initials = "RK", doLogout }) => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <div className={classes.root}>
      <PopupState variant="popover" popupId="demo-popup-popover">
        {(popupState) => (
          <div>
            <Avatar className={classes.purple} {...bindTrigger(popupState)}>
              {initials}
            </Avatar>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "center",
              }}
            >
              <Box p={2}>
                <Button
                  onClick={() => {
                    doLogout({ history });
                  }}
                >
                  Logout
                </Button>
                <Button onClick={() => history.push("/settings")}>
                  Settings
                </Button>
              </Box>
            </Popover>
          </div>
        )}
      </PopupState>
    </div>
  );
};

export default connect(undefined, { doLogout, logoutSuccess })(UserAvatar);
