import { createSelector } from "reselect";

const sharedComponentsSelector = (state) => state.sharedComponents;

export const isSnackbarOpenSelector = createSelector(
  sharedComponentsSelector,
  (state) => state.isSnackbarOpen
);

export const snackbarMessageSelector = createSelector(
  sharedComponentsSelector,
  (state) => state.snackbarMessage
);
