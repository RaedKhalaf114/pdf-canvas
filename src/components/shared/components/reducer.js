import * as ActionTypes from "./actionTypes";

const initialState = {
  snackbarMessage: "",
  isSnackbarOpen: false,
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case ActionTypes.TRIGGER_SNACKBAR:
      return {
        ...state,
        snackbarMessage: payload?.message,
        isSnackbarOpen: payload?.isOpen,
      };
    default:
      return state;
  }
};
