import React from "react";
import Tooltip from "@material-ui/core/Tooltip";
import EditIcon from "@material-ui/icons/Edit";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";

import "./RowActions.scss";

const RowAction = ({ handleRowEdit, handleRowRemove }) => (
  <div className="row-actions">
    <div className="edit-action">
      <Tooltip title="Edit File" aria-label="edit" onClick={handleRowEdit}>
        <EditIcon />
      </Tooltip>
    </div>
    <div className="delete-action">
      <Tooltip
        title="Delete File"
        aria-label="delete"
        onClick={handleRowRemove}
      >
        <DeleteOutlineIcon />
      </Tooltip>
    </div>
  </div>
);

export default RowAction;
