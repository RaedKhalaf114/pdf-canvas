import React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import * as MuiSnackbar from "@material-ui/core/Snackbar";
import { isSnackbarOpenSelector, snackbarMessageSelector } from "../selectors";
import { triggerSnackbar } from "../actions";

const MaterialUISnackbar = MuiSnackbar.default;

const Snackbar = ({ isSnackbarOpen, triggerSnackbar, snackbarMessage }) => {
  return (
    <MaterialUISnackbar
      anchorOrigin={{
        vertical: "top",
        horizontal: "center",
      }}
      open={isSnackbarOpen}
      autoHideDuration={3000}
      onClose={() => triggerSnackbar(false, "")}
      onExited={() => triggerSnackbar(false, "")}
      message={snackbarMessage}
      action={
        <React.Fragment>
          <Button
            color="secondary"
            size="small"
            onClick={() => triggerSnackbar(false, "")}
          >
            close
          </Button>
          <IconButton
            aria-label="close"
            color="inherit"
            onClick={() => triggerSnackbar(false, "")}
          >
            <CloseIcon />
          </IconButton>
        </React.Fragment>
      }
    />
  );
};

const mapStateToProps = (state) => ({
  isSnackbarOpen: isSnackbarOpenSelector(state),
  snackbarMessage: snackbarMessageSelector(state),
});

export default connect(mapStateToProps, { triggerSnackbar })(Snackbar);
