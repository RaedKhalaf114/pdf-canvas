import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Classnames from "classnames";

import "./LoadingOverlay.scss";

const LoadingOverlay = ({ isLoading, isFullScreen = true }) => {
  return (
    <div
      className={Classnames(
        "loading-overlay-container",
        { loading: isLoading },
        { fullScreen: isFullScreen }
      )}
    >
      <CircularProgress color="secondary" className={"spinner"} />
    </div>
  );
};

export default LoadingOverlay;
