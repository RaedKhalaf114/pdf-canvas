import React, { Component, useCallback, useState } from "react";
import { DropzoneDialog } from "material-ui-dropzone";
import Button from "@material-ui/core/Button";

const FileUpload = () => {
  const [open, setOpen] = useState(false);
  const [files, setFiles] = useState([]);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  const handleSave = useCallback((files) => {
    setFiles(files);
    setOpen(false);
  }, []);

  const handleOpen = useCallback(() => {
    setOpen(true);
  }, []);

  return (
    <div className="upload-file-dialog">
      <Button onClick={handleOpen}>Add Image</Button>
      <DropzoneDialog
        open={open}
        onSave={handleSave}
        acceptedFiles={["image/jpeg", "pdf", "image/png"]}
        showPreviews={true}
        maxFileSize={5000000}
        onClose={handleClose}
      />
    </div>
  );
};

export default FileUpload;
