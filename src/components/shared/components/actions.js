import * as ActionTypes from "./actionTypes";

export const triggerSnackbar = (isOpen, message) => ({
  type: ActionTypes.TRIGGER_SNACKBAR,
  payload: { isOpen, message },
});
