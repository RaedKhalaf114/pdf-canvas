import React from "react";
import Typography from "@material-ui/core/Typography";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { useHistory } from "react-router-dom";

import "./Header.scss";
import Avatar from "../Avatar";

const Header = () => {
  const history = useHistory();

  const activePath = history?.location?.pathname;

  const activeTab =
    activePath.indexOf("dashboard") === -1
      ? "/home/settings"
      : "/home/dashboard";

  return (
    <div className="header-container">
      <Typography variant="h6" gutterBottom>
        Augmented AI
      </Typography>
      <div>
        <Tabs
          indicatorColor="primary"
          textColor="primary"
          aria-label="disabled tabs example"
          value={activeTab}
        >
          <Tab
            label="Home"
            value="/home/dashboard"
            onClick={() => history.push("/home/dashboard")}
          />
          <Tab
            label="Settings"
            value="/home/settings"
            onClick={() => history.push("/home/settings")}
          />
        </Tabs>
      </div>
      <div className="user-avatar">
        <Avatar initials="RK" />
      </div>
    </div>
  );
};

export default Header;
