import * as ActionTypes from "./actionTypes";

export const getGroups = (payload) => ({
  type: ActionTypes.GET_GROUPS,
  payload,
});

export const getGroupsSuccess = (payload) => ({
  type: ActionTypes.GET_GROUPS_SUCCESS,
  payload,
});

export const createGroups = (payload) => ({
  type: ActionTypes.CREATE_GROUP,
  payload,
});

export const editGroups = (payload) => ({
  type: ActionTypes.EDIT_GROUP,
  payload,
});

export const deleteGroup = (payload) => ({
  type: ActionTypes.DELETE_GROUP,
  payload,
});

export const triggerLoading = (payload) => ({
  type: ActionTypes.TRIGGER_LOADING,
  payload,
});
