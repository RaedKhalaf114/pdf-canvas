import { createSelector } from "reselect";

export const settingsSelector = (state) => state.settings;

export const groupsSelector = createSelector(
  settingsSelector,
  (state) => state.groups
);

export const isLoadingSelector = createSelector(
  settingsSelector,
  (state) => state.isLoading
);
