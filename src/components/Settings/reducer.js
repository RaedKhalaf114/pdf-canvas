import * as ActionTypes from "./actionTypes";

const initialState = {
  isLoading: false,
  groups: [],
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case ActionTypes.TRIGGER_LOADING:
      return { ...state, isLoading: payload };
    case ActionTypes.GET_GROUPS_SUCCESS:
      return { ...state, groups: payload };
    default:
      return state;
  }
};
