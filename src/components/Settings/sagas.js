import { all, call, takeLatest, put, select } from "redux-saga/effects";
import * as ActionTypes from "./actionTypes";
import * as Actions from "./actions";
import { createGroupsEndpoint, getGroupsEndpoint } from "./endpoints";
import { doDelete, doGet, doPost, doPut } from "../../http/methods";
import { getGroupsSuccess, triggerLoading } from "./actions";
import { triggerSnackbar } from "../shared/components/actions";

function* getGroups() {
  try {
    yield put(triggerLoading(true));
    const response = yield call(doGet, { url: getGroupsEndpoint });
    const groups = response.data;
    yield put(getGroupsSuccess(groups));
  } catch (e) {
    console.error(e);
  } finally {
    yield put(triggerLoading(false));
  }
}

function* createGroup({ payload }) {
  try {
    yield put(triggerLoading(true));
    const response = yield call(doPost, {
      url: createGroupsEndpoint,
      payload: { name: payload.groupName },
    });
    yield put(triggerSnackbar(true, "Group is Added successfully"));
    yield put(Actions.getGroups());
  } catch (e) {
    yield put(triggerSnackbar(true, "Something went wrong, please try again"));
    console.error(e);
  } finally {
    yield put(triggerLoading(false));
    if (payload.onFinish) {
      payload.onFinish();
    }
  }
}

function* editGroup({ payload }) {
  try {
    yield put(triggerLoading(true));
    const response = yield call(doPut, {
      url: createGroupsEndpoint,
      payload: { name: payload.groupName, id: payload.id },
    });
    yield put(triggerSnackbar(true, "Group is Edited successfully"));
    yield put(Actions.getGroups());
  } catch (e) {
    yield put(triggerSnackbar(true, "Something went wrong, please try again"));
    console.error(e);
  } finally {
    yield put(triggerLoading(false));
    if (payload.onFinish) {
      payload.onFinish();
    }
  }
}

function* deleteGroup({ payload }) {
  try {
    yield put(triggerLoading(true));
    const response = yield call(doDelete, {
      url: createGroupsEndpoint,
      payload: { groupId: payload.id },
    });
    yield put(
      triggerSnackbar(true, `Group ${payload?.name} is deleted successfully`)
    );
    yield put(Actions.getGroups());
  } catch (e) {
    yield put(triggerSnackbar(true, "Something went wrong, please try again"));
    console.error(e);
  } finally {
    yield put(triggerLoading(false));
  }
}

export default function* watcher() {
  yield all([
    takeLatest(ActionTypes.DELETE_GROUP, deleteGroup),
    takeLatest(ActionTypes.CREATE_GROUP, createGroup),
    takeLatest(ActionTypes.EDIT_GROUP, editGroup),
    takeLatest(ActionTypes.GET_GROUPS, getGroups),
  ]);
}
