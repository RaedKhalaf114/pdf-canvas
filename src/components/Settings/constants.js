export const SETTINGS_TABs = [
  {
    title: "Groups",
    route: "/groups",
    icon: "/images/batch.png",
  },
];

export const GroupFormMode = {
  EDIT: "Edit",
  NEW: "New",
};
