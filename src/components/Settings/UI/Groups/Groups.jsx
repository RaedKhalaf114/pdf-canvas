import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TablePaginationActions from "../../../shared/components/TablePaginationActions";
import { groupsSelector, isLoadingSelector } from "../../selectors";
import { deleteGroup, getGroups } from "../../actions";
import LoadingOverlay from "../../../shared/components/LoadingOverlay/LoadingOverlay";
import TableHead from "@material-ui/core/TableHead";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import AddGroup from "./AddGroup";
import RowAction from "../../../shared/components/RowActions";
import ConfirmationDialog from "../../../shared/components/ConfirmationDialog";
import { GroupFormMode } from "../../constants";

const ObjectRow = ({ row, editRow, removeRow }) => {
  const [isHover, setHover] = useState(false);

  return (
    <TableRow
      style={{ height: "70px" }}
      key={row.name}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <TableCell component="th" scope="row">
        {row.name}
      </TableCell>
      <TableCell style={{ width: 160 }}>{row.id}</TableCell>
      <TableCell width={100}>
        {isHover && (
          <RowAction
            handleRowRemove={() => removeRow(row)}
            handleRowEdit={() => editRow(row)}
          />
        )}
      </TableCell>
    </TableRow>
  );
};

const Groups = ({ groups: rows, isLoading, getGroups, deleteGroup }) => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [isAddOpen, setAddOpen] = useState(false);
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);
  const [rowToBeRemoved, setRowToBeRemoved] = useState(undefined);
  const [toBeEditedRow, setToBeEditedRow] = useState(undefined);

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const onConfirmDelete = () => {
    setShowConfirmDialog(false);
    deleteGroup(rowToBeRemoved);
  };

  const deleteRow = (row) => {
    setRowToBeRemoved(row);
    setShowConfirmDialog(true);
  };

  const editRow = (row) => {
    setToBeEditedRow(row);
    setAddOpen(true);
  };

  const handleAddObject = () => {
    setAddOpen(true);
  };

  useEffect(() => {
    getGroups();
  }, []);

  return (
    <div>
      <ConfirmationDialog
        isOpen={showConfirmDialog}
        onClose={() => setShowConfirmDialog(false)}
        onConfirm={onConfirmDelete}
        title={`Delete ${rowToBeRemoved?.name} Group`}
        content={`Are you sure you want to delete this Group ? The files under this group will not be removed.`}
      />
      <AddGroup
        onClose={() => {
          setToBeEditedRow(undefined);
          setAddOpen(false);
        }}
        open={isAddOpen}
        row={toBeEditedRow}
        mode={Boolean(toBeEditedRow) ? GroupFormMode.EDIT : GroupFormMode.NEW}
      />
      <LoadingOverlay isLoading={isLoading} isFullScreen={false} />
      <div className="settings-header">
        <div>Groups</div>
        <div>
          <Button
            variant="contained"
            onClick={handleAddObject}
            startIcon={<AddIcon />}
            color="primary"
          >
            Group
          </Button>
        </div>
      </div>
      <TableContainer>
        <Table aria-label="custom pagination table">
          <TableHead className="table-header">
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>ID</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0
              ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              : rows
            ).map((row) => (
              <ObjectRow editRow={editRow} removeRow={deleteRow} row={row} />
            ))}

            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[10]}
                colSpan={3}
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { "aria-label": "rows per page" },
                  native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </div>
  );
};

const mapStateToProps = (state) => ({
  groups: groupsSelector(state),
  isLoading: isLoadingSelector(state),
});

export default connect(mapStateToProps, { getGroups, deleteGroup })(Groups);
