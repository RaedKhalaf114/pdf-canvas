import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import LoadingOverlay from "../../../../shared/components/LoadingOverlay/LoadingOverlay";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import { createGroups, editGroups } from "../../../actions";
import { isLoadingSelector } from "../../../selectors";
import { GroupFormMode } from "../../../constants";

const AddGroup = ({
  open,
  onClose,
  createGroups,
  editGroups,
  isLoading,
  mode = GroupFormMode.NEW,
  row,
}) => {
  const [groupName, setGroupName] = useState("");

  useEffect(() => {
    if (mode === GroupFormMode.EDIT) {
      setGroupName(row.name);
    }
  }, [mode, row]);

  const handleClose = () => {
    setGroupName("");
    onClose();
  };

  const onAddGroupFinishes = () => {
    handleClose();
  };

  const handleAdd = () => {
    if (mode === GroupFormMode.NEW) {
      createGroups({ groupName, onFinish: onAddGroupFinishes });
    } else {
      editGroups({ groupName, id: row.id, onFinish: onAddGroupFinishes });
    }
  };

  return (
    <>
      <Dialog open={open} className="add-group-container">
        <LoadingOverlay isLoading={isLoading} isFullScreen={false} />

        <DialogTitle>
          {mode === GroupFormMode.NEW ? "Add Group" : "Edit Group"}
        </DialogTitle>

        <DialogContent>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            label="Group Name"
            type="text"
            autoFocus
            value={groupName}
            onChange={(e) => setGroupName(e.target?.value)}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={() => handleClose()} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={handleAdd}
            color="primary"
            disabled={!Boolean(groupName)}
          >
            {mode === GroupFormMode.NEW ? "New" : "Edit"}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

const mapStateToProps = (state) => ({
  isLoading: isLoadingSelector(state),
});

export default connect(mapStateToProps, { createGroups, editGroups })(AddGroup);
