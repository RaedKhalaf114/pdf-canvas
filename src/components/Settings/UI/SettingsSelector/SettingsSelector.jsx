import React from "react";
import { SETTINGS_TABs } from "../../constants";
import { useHistory } from "react-router-dom";

import "./SettingsSelector.scss";

const SettingItem = ({ item, onSelectItem }) => (
  <div className="setting-item" onClick={() => onSelectItem(item)}>
    <div className="setting-item-icon">
      <img src={item?.icon} alt={item.title} />
    </div>
    <div className="setting-item-label">{item.title}</div>
  </div>
);

const SettingsSelector = () => {
  const history = useHistory();

  const handleItemSelection = (item) => {
    history.push(`/home/settings${item?.route}`);
  };

  return (
    <>
      <div className="settings-header">Settings</div>
      <div className="settings-selectors">
        {SETTINGS_TABs.map((item) => (
          <SettingItem item={item} onSelectItem={handleItemSelection} />
        ))}
      </div>
    </>
  );
};

export default SettingsSelector;
