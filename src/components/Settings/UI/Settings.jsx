import React from "react";
import { Switch } from "react-router-dom";

import "./Settings.scss";
import { PrivateRoute } from "components/shared/components/PrivateRoute/PrivateRoute";
import SettingsSelector from "./SettingsSelector";
import Container from "@material-ui/core/Container";
import Groups from "./Groups";

const Settings = () => {
  return (
    <Container className="settings-container" maxWidth="lg">
      <Switch>
        <PrivateRoute
          path="/home/settings"
          exact
          component={SettingsSelector}
        />
        <PrivateRoute path="/home/settings/groups" component={Groups} />
      </Switch>
    </Container>
  );
};

export default Settings;
