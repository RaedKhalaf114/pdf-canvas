export const GET_GROUPS = "Get Groups";
export const GET_GROUPS_SUCCESS = "Get Groups Success";

export const CREATE_GROUP = "Create Group";
export const DELETE_GROUP = "Delete Group";
export const EDIT_GROUP = "Edit Group";

export const TRIGGER_LOADING = "Trigger Loading Groups";
