export const getGroupsEndpoint = "/group/";
export const createGroupsEndpoint = "/group/";
export const deleteGroupsEndpoint = "/group/";
export const updateGroupsEndpoint = "/group/";
