export const DO_LOGIN = "Login";
export const LOGIN_FAILED = "Login failed";
export const LOGIN_SUCCESS = "Login Success";

export const DO_SIGN_UP = "Signup";
export const SIGN_UP_FAILED = "Signup failed";
export const SIGN_UP_SUCCESS = "Signup success";

export const DO_LOGOUT = "Logout";
export const LOGOUT_SUCCESS = "Logout success";
export const LOGOUT_FAILED = "Logout failed";
export const RESET_AUTH_STATUS = "Reset auth status";
