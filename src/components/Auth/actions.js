import * as ActionTypes from "./actionTypes";

export const doLogin = (payload) => ({
  type: ActionTypes.DO_LOGIN,
  payload,
});

export const loginFailed = (payload) => ({
  type: ActionTypes.LOGIN_FAILED,
  payload,
});

export const loginSuccess = (payload) => ({
  type: ActionTypes.LOGIN_SUCCESS,
  payload,
});

export const doSignUp = (payload) => ({
  type: ActionTypes.DO_SIGN_UP,
  payload,
});

export const signUpFailed = (payload) => ({
  type: ActionTypes.SIGN_UP_FAILED,
  payload,
});

export const signUpSuccess = (payload) => ({
  type: ActionTypes.SIGN_UP_SUCCESS,
  payload,
});

export const doLogout = (payload) => ({
  type: ActionTypes.DO_LOGOUT,
  payload,
});

export const logoutFailed = (payload) => ({
  type: ActionTypes.LOGOUT_FAILED,
  payload,
});

export const logoutSuccess = (payload) => ({
  type: ActionTypes.LOGOUT_SUCCESS,
  payload,
});

export const resetAuthStatus = (payload) => ({
  type: ActionTypes.RESET_AUTH_STATUS,
  payload,
});
