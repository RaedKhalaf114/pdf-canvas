export const AUTH_STATUS = {
  PENDING: "pending",
  SUCCESS: "success",
  FAILED: "failed",
};
