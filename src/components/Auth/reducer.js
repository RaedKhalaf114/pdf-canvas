import * as ActionTypes from "./actionTypes";
import { AUTH_STATUS } from "./constants";

const initialState = {
  isLoading: false,
  authStatus: AUTH_STATUS.PENDING,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.DO_LOGIN:
      return { ...state, isLoading: true };
    case ActionTypes.DO_LOGOUT:
      return { ...state, isLoading: true };
    case ActionTypes.LOGOUT_SUCCESS:
      return { ...state, isLoading: false };
    case ActionTypes.LOGIN_SUCCESS:
      return { ...state, isLoading: false, authStatus: AUTH_STATUS.SUCCESS };
    case ActionTypes.LOGIN_FAILED:
      return {
        ...state,
        isLoading: false,
        authStatus: AUTH_STATUS.FAILED,
        error: action.payload,
      };
    case ActionTypes.DO_SIGN_UP:
      return { ...state, isLoading: true };
    case ActionTypes.SIGN_UP_FAILED:
      return {
        ...state,
        isLoading: false,
        authStatus: AUTH_STATUS.FAILED,
        error: action.payload,
      };
    case ActionTypes.SIGN_UP_SUCCESS:
      return { ...state, isLoading: false, authStatus: AUTH_STATUS.SUCCESS };
    case ActionTypes.RESET_AUTH_STATUS:
      return { ...state, authStatus: null, error: null };
    default:
      return state;
  }
};
