import { all, call, takeLatest, put } from "redux-saga/effects";

import * as ActionTypes from "./actionTypes";
import { doPost } from "../../http/methods";
import { loginEndpoint, logoutEndpoint, signUpEndpoint } from "./endpoints";
import {
  loginFailed,
  loginSuccess,
  logoutSuccess,
  signUpFailed,
  signUpSuccess,
} from "./actions";

function* doLogin({ payload }) {
  const { username, password } = payload;

  try {
    const response = yield call(doPost, {
      url: loginEndpoint,
      payload: { username, password },
    });

    if (payload?.handleSuccessLogin) {
      payload.handleSuccessLogin(response);
    }

    yield put(loginSuccess(response));
  } catch (e) {
    if (e?.response?.status) {
      yield put(loginFailed("Invalid username or password"));
    } else {
      yield put(loginFailed("something went wrong"));
    }
  }
}

function* doSignup({ payload }) {
  const { username, email, password } = payload;

  try {
    const response = yield call(doPost, {
      url: signUpEndpoint,
      payload: { username, email, password },
    });

    if (payload?.handleSuccessLogin) {
      payload.handleSuccessLogin(response);
    }

    yield put(signUpSuccess(response));
  } catch (e) {
    yield put(signUpFailed("something went wrong"));
  }
}

function* logout({ payload }) {
  try {
    const response = yield call(doPost, { url: logoutEndpoint });

    payload?.history?.push("/auth/login");

    yield put(logoutSuccess());
  } catch (e) {
    console.log(e);
  }
}

export default function* watcher() {
  yield all([
    takeLatest(ActionTypes.DO_LOGIN, doLogin),
    takeLatest(ActionTypes.DO_SIGN_UP, doSignup),
    takeLatest(ActionTypes.DO_LOGOUT, logout),
  ]);
}
