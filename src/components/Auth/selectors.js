import { createSelector } from "reselect";

export const authStateSelector = (state) => state.auth;

export const isLoadingSelector = createSelector(
  authStateSelector,
  (state) => state.isLoading
);

export const authStatusSelector = createSelector(
  authStateSelector,
  (state) => state.authStatus
);

export const authErrorSelector = createSelector(
  authStateSelector,
  (state) => state.error
);
