import React, { useEffect, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Link as RouterLink } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import {
  authErrorSelector,
  authStatusSelector,
  isLoadingSelector,
} from "../../selectors";
import { doSignUp, resetAuthStatus } from "../../actions";

import LoadingOverlay from "../../../shared/components/LoadingOverlay/LoadingOverlay";
import { AUTH_STATUS } from "../../constants";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUp = ({
  authStatus,
  doSignUp,
  isLoading,
  error,
  resetAuthStatus,
}) => {
  const classes = useStyles();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    const reqBody = { username: name, email, password, history };
    doSignUp(reqBody);
  };

  useEffect(() => {
    if (authStatus === AUTH_STATUS.SUCCESS) {
      resetAuthStatus();
      history.push("/auth/login");
    }
  }, [authStatus]);

  return (
    <Container component="main" maxWidth="xs" className="sign-up-container">
      <LoadingOverlay isLoading={isLoading} />

      <CssBaseline />

      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>

        <Typography component="h1" variant="h5">
          Sign up
        </Typography>

        {Boolean(error) && (
          <Alert style={{ marginTop: "15px" }} severity="error">
            {error}
          </Alert>
        )}

        <form className={classes.form} onSubmit={(e) => handleSubmit(e)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="name"
            label="Name"
            type="text"
            id="name"
            autoComplete="name"
            autoFocus
            onChange={(e) => setName(e.target?.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            onChange={(e) => setEmail(e.target?.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) => setPassword(e.target?.value)}
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign up
          </Button>

          <Grid container>
            <Grid item>
              <RouterLink to="/auth/login">
                Already have an account? Sign in
              </RouterLink>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

const mapStateToProps = (state) => ({
  isLoading: isLoadingSelector(state),
  authStatus: authStatusSelector(state),
  error: authErrorSelector(state),
});

export default connect(mapStateToProps, { doSignUp, resetAuthStatus })(SignUp);
