import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Link as RouterLink } from "react-router-dom";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";

import {
  authErrorSelector,
  authStatusSelector,
  isLoadingSelector,
} from "../../selectors";
import LoadingOverlay from "../../../shared/components/LoadingOverlay/LoadingOverlay";
import { doLogin, resetAuthStatus } from "../../actions";
import { AUTH_STATUS } from "../../constants";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = ({ authStatus, isLoading, doLogin, error, resetAuthStatus }) => {
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    doLogin({ username, password, history, handleSuccessLogin });
  };

  const handleSuccessLogin = () => {};

  useEffect(() => {
    if (authStatus === AUTH_STATUS.SUCCESS) {
      resetAuthStatus();
      history.push("/home");
    }
  }, [authStatus]);

  return (
    <Container component="main" maxWidth="xs" className="login-container">
      <LoadingOverlay isLoading={isLoading} />

      <CssBaseline />

      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>

        <Typography component="h1" variant="h5">
          Sign in
        </Typography>

        {Boolean(error) && (
          <Alert style={{ marginTop: "15px" }} severity="error">
            {error}
          </Alert>
        )}

        <form className={classes.form} onSubmit={(e) => handleSubmit(e)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            autoFocus
            onChange={(e) => setUsername(e.target?.value)}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) => setPassword(e.target?.value)}
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item>
              <RouterLink to="/auth/sign-up">
                Don't have an account? Sign Up
              </RouterLink>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

const mapStateToProps = (state) => ({
  isLoading: isLoadingSelector(state),
  authStatus: authStatusSelector(state),
  error: authErrorSelector(state),
});

export default connect(mapStateToProps, { doLogin, resetAuthStatus })(Login);
