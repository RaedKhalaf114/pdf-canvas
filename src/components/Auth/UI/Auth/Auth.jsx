import React, { useEffect } from "react";

import { Switch, Route, Redirect } from "react-router-dom";

import "./Auth.scss";
import Login from "../Login/Login";
import SignUp from "../Signup/Signup";

const Auth = () => {
  return (
    <div className="auth-container">
      <Switch>
        <Route path="/auth/login" component={Login} />
        <Route path="/auth/sign-up" component={SignUp} />
        <Route
          path="/auth"
          exact
          render={() => <Redirect to="/auth/login" />}
        />
        <Route path="/*" render={() => <Redirect to="/auth/login" />} />
      </Switch>
    </div>
  );
};

export default Auth;
