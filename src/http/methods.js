import axios from "axios";
import { injectCommonHeaders } from "./utils";

const axiosInstance = axios.create();
axiosInstance.defaults.withCredentials = true;
axiosInstance.defaults.xsrfCookieName = "csrftoken";
axiosInstance.defaults.xsrfHeaderName = "X-CSRFToken";

axiosInstance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (
      error?.response?.status === 401 &&
      window.location.href.indexOf("login") === -1
    ) {
      window.location.replace(window.location.origin + "/auth/login");
    }
    return Promise.reject(error);
  }
);

export const doPost = ({
  url,
  payload = {},
  headers = {},
  options = {},
  baseURL = process.env.REACT_APP_BASE_URL,
}) => {
  return axiosInstance.post(url, payload, {
    baseURL,
    headers: injectCommonHeaders(headers),
    ...options,
  });
};

export const doPut = ({
  url,
  payload,
  headers = {},
  options = {},
  baseURL = process.env.REACT_APP_BASE_URL,
}) => {
  return axiosInstance.put(url, payload, {
    baseURL,
    headers: injectCommonHeaders(headers),
    ...options,
  });
};

export const doGet = ({
  url,
  headers = {},
  options = {},
  baseURL = process.env.REACT_APP_BASE_URL,
}) => {
  return axiosInstance.get(url, {
    baseURL,
    headers: injectCommonHeaders(headers),
    ...options,
  });
};

export const doDelete = ({
  url,
  payload,
  headers = {},
  options = {},
  baseUrl = process.env.REACT_APP_BASE_URL,
}) =>
  axiosInstance({
    url,
    baseURL: baseUrl,
    method: "delete",
    data: payload,
    headers: injectCommonHeaders(headers),
    ...options,
  });
