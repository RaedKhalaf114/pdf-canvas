export const injectCommonHeaders = (headers) => {
  return {
    "Content-Type": "application/json",
    ...headers,
  };
};
